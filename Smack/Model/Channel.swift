//
//  Channel.swift
//  Smack
//
//  Created by cjnora on 2018/1/22.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import Foundation

struct Channel : Decodable {
    public private(set) var _id: String!
    public private(set) var name: String!
    public private(set) var description: String!
    public private(set) var __v: Int?
}
